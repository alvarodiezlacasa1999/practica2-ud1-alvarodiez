package com.alvarodiez.vehiculosmvc.gui;

import com.alvarodiez.vehiculosmvc.base.Articulo;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

public class Ventana {
    /**
     * @author Alvaro Diez
     * @since 1.5
     * @version 1.1
     */
    public JFrame frame;
    public JPanel panel;

    public JRadioButton ropaRadioButton;
    public JRadioButton calzadoRadioButton;

    public JTextField prendaTxt;
    public JTextField marcaTxt;
    public JTextField modeloTxt;
    public JTextField tallaTxt;
    public JTextField telaSuelaTxt;
    public JTextField codigoTxt;

    public JButton nuevoBtn;
    public JButton exportarXMLBtn;
    public JButton importarXMLBtn;

    public JLabel articuloLbl;
    public JLabel prendaLbl;
    public JLabel marcaLbl;
    public JLabel modeloLbl;
    public JLabel tallaLbl;
    public JLabel telaSuelaLbl;
    public JLabel codigoLbl;
    public JLabel fechaLbl;
    
    public DatePicker fechaDP;
    public JList list1;

    //Elementos creados por mi
    public DefaultListModel<Articulo> dlm;

    //Constructores

    public Ventana() {
        frame = new JFrame("ROPA MVC");
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    //Métodos

    /**
     * Método para establece el modelo del componente lista:list1
     */
    private void initComponents() {
        dlm=new DefaultListModel<Articulo>();
        list1.setModel(dlm);
    }

}
