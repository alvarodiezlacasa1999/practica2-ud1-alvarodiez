package com.alvarodiez.vehiculosmvc.gui;

import com.alvarodiez.vehiculosmvc.base.Articulo;
import com.alvarodiez.vehiculosmvc.base.Calzado;
import com.alvarodiez.vehiculosmvc.base.Ropa;
import com.alvarodiez.vehiculosmvc.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.Properties;

public class RopaControlador implements ActionListener, ListSelectionListener, WindowListener {
    /**
     * @author Alvaro Diez
     * @since 1.5
     * @version 1.1
     */
    private Ventana vent;
    private RopaModelo modelo;
    private File ultimaRuta;

    //Constructores

    /**
     * Parámetros de la clase ropaControlador
     * @param vent
     * @param modelo
     */
    public RopaControlador(Ventana vent, RopaModelo modelo) {
        this.vent = vent;
        this.modelo = modelo;

        try {
            //Carga la configuración
            cargarDatosConfiguracion();
        } catch (IOException e) {
            e.printStackTrace();
        }

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);
    }

    //Métodos

    /**
     * Método que establece las funciones de los botones
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        switch(actionCommand){
            case "Nuevo":
                if (hayCamposVacios()) {
                    Util.mensajeError("No puede haber campos vacios");
                    vent.telaSuelaLbl.getText();
                    break;
                }

                if (modelo.codigoLibre(vent.codigoTxt.getText())) {
                    Util.mensajeError("Código duplicado, introduce otro");
                    vent.codigoTxt.getText();
                    break;
                }

                if (vent.ropaRadioButton.isSelected()) {
                    modelo.altaRopa(vent.prendaTxt.getText(), vent.codigoTxt.getText(), vent.marcaTxt.getText(),
                            vent.modeloTxt.getText(), vent.fechaDP.getDate(), vent.telaSuelaTxt.getText(), vent.tallaTxt.getText());
                }

                if (vent.calzadoRadioButton.isSelected()) {
                    modelo.altaCalzado(vent.prendaTxt.getText(), vent.codigoTxt.getText(), vent.marcaTxt.getText(),
                            vent.modeloTxt.getText(), vent.fechaDP.getDate(),vent.telaSuelaTxt.getText(), vent.tallaTxt.getText());
                }
                limpiar();
                refresh();
                break;

            case "Exportar XML":
                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(ultimaRuta, "Archivos XML", "xml");
                int opt2 = selectorFichero2.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;

            case "Importar XML":
                JFileChooser selectorFichero = Util.crearSelectorFicheros(ultimaRuta, "Archivo XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                    refresh();
                }
                break;

            case "Ropa":
                vent.telaSuelaLbl.setText("Tipo de material");
                break;
            case "Calzado":
                vent.telaSuelaLbl.setText("Tipo de suela");
                break;
        }
    }

    /**
     * Método que actualiza la lista
     */
    private void refresh() {
        vent.dlm.clear();
        for (Articulo unArticulo: modelo.getLista()) {
            vent.dlm.addElement(unArticulo);
        }
    }

    /**
     * Método que borra todos los parámetros menos código
     */
    private void limpiar() {
        vent.prendaTxt.setText(null);
        vent.tallaTxt.setText(null);
        vent.fechaDP.setText(null);
        vent.modeloTxt.setText(null);
        vent.marcaTxt.setText(null);
        vent.telaSuelaTxt.setText(null);
        vent.codigoTxt.requestFocus();
    }

    /**
     * Método que dice si hay algún campo vacio
     * @return
     */
    private boolean hayCamposVacios() {
        if (vent.telaSuelaTxt.getText().isEmpty() ||
                vent.marcaTxt.getText().isEmpty() ||
                vent.modeloTxt.getText().isEmpty() ||
                vent.codigoTxt.getText().isEmpty() ||
                vent.fechaDP.getText().isEmpty() ||
                vent.tallaTxt.getText().isEmpty() ||
                vent.prendaTxt.getText().isEmpty() ) {
            return true;
        }
        return false;
    }

    /**
     * Método para agregar funciones a los botones y los radioButtons
     * @param listener
     */
    public void addActionListener(ActionListener listener) {
        vent.ropaRadioButton.addActionListener(listener);
        vent.calzadoRadioButton.addActionListener(listener);
        vent.exportarXMLBtn.addActionListener(listener);
        vent.importarXMLBtn.addActionListener(listener);
        vent.nuevoBtn.addActionListener(listener);
    }

    /**
     * Añade función a la lista
     * @param listener
     */
    public void addListSelectionListener(ListSelectionListener listener) {
        vent.list1.addListSelectionListener(listener);
    }

    /**
     * Añade función a la ventana
     * @param listener
     */
    public void addWindowListener(WindowListener listener) {
        vent.frame.addWindowListener(listener);
    }

    /**
     * Guarda la configuración en la ruta que hayamos elegido (ropa.conf)
     * @throws IOException
     */
    private void guardarConfiguracion() throws IOException {
        Properties conf = new Properties();
        conf.setProperty("ultimaRuta", ultimaRuta.getAbsolutePath());
        conf.store(new PrintWriter("ropa.conf"), "Configuración Ropa");
    }

    /**
     * Actualiza la ruta
     * @param ultimaRuta
     */
    private void actualizarDatosConfiguracion(File ultimaRuta) {
        this.ultimaRuta = ultimaRuta;
    }

    /**
     * Método para cargar la configuración y establecer la ruta de destino
     * @throws IOException
     */
    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("Ropa.conf"));
        ultimaRuta = new File(configuracion.getProperty("ultimaRuta"));
    }

    /**
     * Método que muestra un mensaje para confirmar que queremos salir del programa,
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {int resp = Util.mensajeConfirmacion("¿Estás seguro de que quieres salir?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            try {
                //Guarda la configuración
                guardarConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }
    }

    /**
     * Método que actualiza todos los campos/parámetros de la lista
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            Articulo articuloSelecc = (Articulo) vent.list1.getSelectedValue();
            vent.codigoTxt.setText((articuloSelecc.getCodigo()));
            vent.marcaTxt.setText(articuloSelecc.getMarca());
            vent.modeloTxt.setText(articuloSelecc.getModelo());
            vent.fechaDP.setDate(articuloSelecc.getFechaGarantia());
            vent.tallaTxt.setText(articuloSelecc.getTalla());
            vent.prendaTxt.setText(articuloSelecc.getTipoPrenda());

            if (articuloSelecc instanceof Ropa) {
                vent.ropaRadioButton.doClick();
                vent.telaSuelaTxt.setText(String.valueOf(((Ropa) articuloSelecc).getTipoTela()));
            } else {
                vent.calzadoRadioButton.doClick();
                vent.telaSuelaTxt.setText(String.valueOf(((Calzado) articuloSelecc).getTipoSuela()));
            }
        }
    }

    @Override
    public void windowOpened(WindowEvent e) { }

    @Override
    public void windowClosed(WindowEvent e) { }

    @Override
    public void windowIconified(WindowEvent e) { }

    @Override
    public void windowDeiconified(WindowEvent e) { }

    @Override
    public void windowActivated(WindowEvent e) { }

    @Override
    public void windowDeactivated(WindowEvent e) { }
}

