package com.alvarodiez.vehiculosmvc.gui;

import com.alvarodiez.vehiculosmvc.base.Ropa;
import com.alvarodiez.vehiculosmvc.base.Calzado;
import com.alvarodiez.vehiculosmvc.base.Articulo;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class RopaModelo {
    /**
     * @author Alvaro Diez
     * @since 1.5
     * @version 1.1
     */
    private ArrayList<Articulo> lista;

    //Constructores
    public RopaModelo() {
        lista = new ArrayList<Articulo>();
    }

    /**
     * Devuelve los valores del ArrayList Artículo a la lista
     * @return lista
     */
    public ArrayList<Articulo> getLista() {
        return lista;
    }

    //Métodos

    /**
     * Método para dar de alta un objeto de la clase Ropa
     * @param tipoPrenda
     * @param codigo
     * @param marca
     * @param modelo
     * @param fechaGarantia
     * @param tipoTela
     * @param talla
     */
    public void altaRopa(String tipoPrenda, String codigo, String marca, String modelo,
                         LocalDate fechaGarantia, String tipoTela, String talla) {
        Ropa ropa = new Ropa(codigo, marca, modelo, fechaGarantia, tipoTela, talla, tipoPrenda);
        lista.add(ropa);
    }

    /**
     * Método para dar de alta un objeto de la clase Calazado
     */
     public void altaCalzado(String prenda, String codigo, String marca, String modelo,
                            LocalDate fechaGarantia, String tipoSuela, String talla){
        Calzado calzado = new Calzado(codigo, marca, modelo, fechaGarantia, tipoSuela, talla, prenda);
        lista.add(calzado);
    }

    /**
     * Método que devuelve si el valor Código está en uso o está libre
     * @param codigo
     * @return codigo
     */
    public boolean codigoLibre(String codigo) {
        for (Articulo articulo: lista) {
            if (articulo.getCodigo().equals(codigo)){
                return true;
            }
        }
        return false;
    }

    /**
     * Método que crea un fichero y lo guarda como un archivo .xml
     * @param fichero
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        //Creamos el elemento raiz (Artículos)
        Element raiz = documento.createElement("Articulos");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoArticulo = null;
        Element nodoDatos = null;
        Text texto = null;

        for (Articulo articulo : lista) {

            //Añado dentro de la etiqueta raiz (Articulos) una etiqueta dependiendo
            // del tipo de articulo que este almacenando (Ropa o calzado)
            if (articulo instanceof Ropa) {
                nodoArticulo = documento.createElement("Ropa-articulo");

            } else {
                nodoArticulo = documento.createElement("Calzado-articulo");
            }
            raiz.appendChild(nodoArticulo);

            //Dentro de la etiqueta artículo le añado las subetiquetas
            //con los datos de sus atributos (tipoPrenda, marca,
            // modelo, talla, codigo, fechaGarantia)
            nodoDatos = documento.createElement("tipo-prenda");
            nodoArticulo.appendChild(nodoDatos);
            texto = documento.createTextNode(articulo.getTipoPrenda());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("marca");
            nodoArticulo.appendChild(nodoDatos);
            texto = documento.createTextNode(articulo.getMarca());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("modelo");
            nodoArticulo.appendChild(nodoDatos);
            texto = documento.createTextNode(articulo.getModelo());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("talla");
            nodoArticulo.appendChild(nodoDatos);
            texto = documento.createTextNode(articulo.getTalla());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("codigo");
            nodoArticulo.appendChild(nodoDatos);
            texto = documento.createTextNode(articulo.getCodigo());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("garantia");
            nodoArticulo.appendChild(nodoDatos);
            texto = documento.createTextNode(articulo.getFechaGarantia().toString());
            nodoDatos.appendChild(texto);

            //Hay un dato que depende del tipo de artículo, debo acceder a él controlando el tipo de objeto
            if (articulo instanceof Ropa) {
                nodoDatos = documento.createElement("tipo-tela");
                nodoArticulo.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Ropa) articulo).getTipoTela()));
                nodoDatos.appendChild(texto);
            } else {
                nodoDatos = documento.createElement("tipo-suela");
                nodoArticulo.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Calzado) articulo).getTipoSuela()));
                nodoDatos.appendChild(texto);
            }
        }

        //Guardo los datos en "fichero" que es el objeto File recibido por parametro
        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);
    }

    /**
     * Método para abrir un archivo .xml
     * @param fichero
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        lista = new ArrayList<Articulo>();
        Ropa nropa = null;
        Calzado ncalzado = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoArticulo = (Element) listaElementos.item(i);

            if (nodoArticulo.getTagName().equals("Ropa-articulo")) {
                nropa = new Ropa();
                nropa.setTipoPrenda(nodoArticulo.getChildNodes().item(0).getTextContent());
                nropa.setMarca(nodoArticulo.getChildNodes().item(1).getTextContent());
                nropa.setModelo(nodoArticulo.getChildNodes().item(2).getTextContent());
                nropa.setTalla(nodoArticulo.getChildNodes().item(3).getNodeValue());
                nropa.setTipoTela(nodoArticulo.getChildNodes().item(4).getTextContent());
                nropa.setCodigo(nodoArticulo.getChildNodes().item(5).getTextContent());
                nropa.setFechaGarantia(LocalDate.parse(nodoArticulo.getChildNodes().item(6).getTextContent()));
                
                lista.add(nropa);
            } else {

                if (nodoArticulo.getTagName().equals("Calzado-articulo")) {
                   ncalzado = new Calzado();
                   ncalzado.setTipoPrenda(nodoArticulo.getChildNodes().item(0).getTextContent());
                   ncalzado.setMarca(nodoArticulo.getChildNodes().item(1).getTextContent());
                   ncalzado.setModelo(nodoArticulo.getChildNodes().item(2).getTextContent());
                   ncalzado.setTalla(nodoArticulo.getChildNodes().item(3).getNodeValue());
                   ncalzado.setCodigo(nodoArticulo.getChildNodes().item(4).getTextContent());
                   ncalzado.setTipoSuela(nodoArticulo.getChildNodes().item(5).getTextContent());
                   ncalzado.setFechaGarantia(LocalDate.parse(nodoArticulo.getChildNodes().item(6).getTextContent()));

                    lista.add(ncalzado);
                }
            }
        }
    }
}
