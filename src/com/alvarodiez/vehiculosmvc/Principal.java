package com.alvarodiez.vehiculosmvc;

import com.alvarodiez.vehiculosmvc.gui.RopaControlador;
import com.alvarodiez.vehiculosmvc.gui.RopaModelo;
import com.alvarodiez.vehiculosmvc.gui.Ventana;

public class Principal {
    /**
     * @author Alvaro Diez
     * @since 1.5
     * @version 1.1
     */

    public static void main(String[] args) {
        //Creamos la ventana
        Ventana vista = new Ventana();
        //Creamos el modelo
        RopaModelo modelo = new RopaModelo();
        //Creamos el controlador que añadirá a vista
        //la función de los métodos de modelo
        RopaControlador controlador = new RopaControlador(vista, modelo);
    }
}
