package com.alvarodiez.vehiculosmvc.util;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

public class Util {
    /**
     * @author Alvaro Diez
     * @since 1.5
     * @version 1.1
     */

    // Métodos

    /**
     * Método que muestra en una ventana un mensaje de error
     * @param mensaje
     */
    public static void mensajeError(String mensaje) {
        JOptionPane.showMessageDialog(null,mensaje,"Error",JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Método que muestra una de ventana para confirmar que queremos salir del programa
     * @param mensaje
     * @param titulo
     * @return
     */
    public static int mensajeConfirmacion(String mensaje, String titulo) {
        return JOptionPane.showConfirmDialog(null,mensaje,titulo,JOptionPane.YES_NO_OPTION);
    }

    /**
     * Método que se encarga de elegir la ruta donde se va a guardar el fichero
     * @param rutaDefecto
     * @param tipoArchivo
     * @param extension
     * @return selectorFichero
     */
    public static JFileChooser crearSelectorFicheros(File rutaDefecto, String tipoArchivo, String extension) {
        JFileChooser selectorFichero = new JFileChooser();
        //Elegir la ruta para el fichero
        if (rutaDefecto!=null) {
            //Elige la ruta actual del directorio
            selectorFichero.setCurrentDirectory(rutaDefecto);
        }
        if (extension!=null) {
            //Crea el fichero .xml
            FileNameExtensionFilter filtro = new FileNameExtensionFilter(tipoArchivo,extension);
            selectorFichero.setFileFilter(filtro);
        }
        return selectorFichero;
    }
}

