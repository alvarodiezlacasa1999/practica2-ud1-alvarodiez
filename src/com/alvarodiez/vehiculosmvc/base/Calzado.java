package com.alvarodiez.vehiculosmvc.base;

import java.time.LocalDate;

public class Calzado extends Articulo {
    /**
     * @author Alvaro Diez
     * @since 1.5
     * @version 1.1
     */
    private String tipoSuela;

    //Constructores

    /**
     * Parámetros de la clase Calzado. Herencia de Artículo.
     * @param codigo
     * @param marca
     * @param modelo
     * @param fechaGarantia
     * @param tipoSuela
     * @param talla
     * @param tipoPrenda
     */
    public Calzado(String codigo, String marca, String modelo, LocalDate fechaGarantia, String tipoSuela, String talla, String tipoPrenda) {
        super(codigo, marca, modelo, fechaGarantia, talla, tipoPrenda);
        this.tipoSuela = tipoSuela;
    }

    public Calzado(){
        super();
    }

    //Getters y setters

    /**
     * Devuelve el tipo de suela
     * @return tipoSuela
     */
    public String getTipoSuela() { return tipoSuela; }

    /**
     * Establece el tipo de suela
     * @param tipoSuela
     */
    public void setTipoSuela(String tipoSuela) { this.tipoSuela = tipoSuela; }

    /**
     * Devuelve todos los valores de la clase Calzado
     * @return tipoPrenda, marca, modelo, talla, tipoSuela, codigo, fechaGarantia
     */
    @Override
    public String toString() {
        return getTipoPrenda() +" "+ getMarca() +" "+ getModelo() +" "+ getTalla() + " "
                + "suela: "+ tipoSuela +"Nº de identificación: " + getCodigo() + ". "
                + "Expiración de la garantia: " + getFechaGarantia();
    }
}
