package com.alvarodiez.vehiculosmvc.base;

import java.time.LocalDate;

public class Ropa extends Articulo {
    /**
     * @author Alvaro Diez
     * @since 1.5
     * @version 1.1
     */
    private String tipoTela;

    //Constructores

    /**
     * Parámetros de la clase Ropa. Herencia de Artículo.
     * @param codigo
     * @param marca
     * @param modelo
     * @param fechaGarantia
     * @param tipoTela
     * @param talla
     * @param tipoPrenda
     */
    public Ropa(String codigo, String marca, String modelo, LocalDate fechaGarantia, String tipoTela, String talla, String tipoPrenda) {
        super(codigo, marca, modelo, fechaGarantia, talla, tipoPrenda);
        this.tipoTela = tipoTela;
    }

    public Ropa(){
        super();
    }

    //Getters y setters

    /**
     * Devuelve el tipo de tela
     * @return tipoTela
     */
    public String getTipoTela() { return tipoTela; }

    /**
     * Establece el tipo de tela
     * @param tipoTela
     */
    public void setTipoTela(String tipoTela) { this.tipoTela = tipoTela; }

    /**
     * Devuelve todos los valores de la clase Ropa
     * @return tipoPrenda, marca, modelo, talla, tipoTela, codigo, fechaGarantia
     */
    @Override
    public String toString() {
        return getTipoPrenda() +" "+ getMarca() +" "+ getModelo() +" "+ getTalla() + " "
                + "Material: "+ tipoTela +"Nº de identificación: " + getCodigo() + ". "
                + "Expiración de la garantia: " + getFechaGarantia();
    }

}

