package com.alvarodiez.vehiculosmvc.base;

import java.time.LocalDate;

public abstract class Articulo {
    /**
     * @author Alvaro Diez
     * @since 1.5
     * @version 1.1
     */
    private String tipoPrenda;
    private String marca;
    private String modelo;
    private String talla;
    private String codigo;
    private LocalDate fechaGarantia;

    //Constructores

    /**
     * Parámetros de la clase Artículo
     * @param codigo
     * @param marca
     * @param modelo
     * @param fechaGarantia
     * @param talla
     * @param tipoPrenda
     */
    public Articulo(String codigo, String marca, String modelo, LocalDate fechaGarantia, String talla, String tipoPrenda) {
        this.codigo = codigo;
        this.marca = marca;
        this.modelo = modelo;
        this.talla = talla;
        this.tipoPrenda = tipoPrenda;
        this.fechaGarantia = fechaGarantia;
    }

    public Articulo(){  }

    //Getters y setters

    /**
     * Devuelve el tipo de prenda
     * @return tipoPrenda
     */
    public String getTipoPrenda() { return tipoPrenda; }

    /**
     * Establece el tipo de la prenda
     * @param tipoPrenda
     */
    public void setTipoPrenda(String tipoPrenda) { this.tipoPrenda = tipoPrenda; }

    /**
     * Devuelve la marca
     * @return marca
     */
    public String getMarca() { return marca; }

    /**
     * Establece la marca
     * @param marca
     */
    public void setMarca(String marca) { this.marca = marca; }

    /**
     * Devuelve el tipo de prenda
     * @return modelo
     */
    public String getModelo() { return modelo; }

    /**
     * Establece el modelo
     * @param modelo
     */
    public void setModelo(String modelo) { this.modelo = modelo; }

    /**
     * Devuelve el tipo de prenda
     * @return talla
     */
    public String getTalla() { return talla; }

    /**
     * Establece la talla
     * @param talla
     */
    public void setTalla(String talla) { this.talla = talla; }

    /**
     * Devuelve el tipo de prenda
     * @return codigo
     */
    public String getCodigo() { return codigo; }

    /**
     * Establece el código
     * @param codigo
     */
    public void setCodigo(String codigo) { this.codigo = codigo; }

    /**
     * Devuelve el tipo de prenda
     * @return fechaGarantia
     */
    public LocalDate getFechaGarantia() { return fechaGarantia; }

    /**
     * Establece la fecha de garantía
     * @param fechaGarantia
     */
    public void setFechaGarantia(LocalDate fechaGarantia) { this.fechaGarantia = fechaGarantia; }
}
